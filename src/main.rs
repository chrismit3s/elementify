use std::vec::Vec;
use std::io;


const SYMBOLS: [&str; 119] = ["_",
    "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al",
    "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe",
    "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr",
    "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn",
    "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm",
    "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W",
    "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn",
    "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf",
    "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds",
    "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og"];

const ELEMENTS: [&str; 119] = ["<missing>",
    "Wasserstoff", "Helium", "Lithium", "Beryllium", "Bor", "Kohlenstoff",
    "Stickstoff", "Sauerstoff", "Fluor", "Neon", "Natrium", "Magnesium",
    "Aluminium", "Silicium", "Phosphor", "Schwefel", "Chlor", "Argon",
    "Kalium", "Calcium", "Scandium", "Titan", "Vanadium", "Chrom", "Mangan",
    "Eisen", "Cobalt", "Nickel", "Kupfer", "Zink", "Gallium", "Germanium",
    "Arsen", "Selen", "Brom", "Krypton", "Rubidium", "Strontium", "Yttrium",
    "Zirconium", "Niob", "Molybdän", "Technetium", "Ruthenium", "Rhodium",
    "Palladium", "Silber", "Cadmium", "Indium", "Zinn", "Antimon", "Tellur",
    "Iod", "Xenon", "Caesium", "Barium", "Lanthan", "Cer", "Praseodym",
    "Neodym", "Promethium", "Samarium", "Europium", "Gadolinium", "Terbium",
    "Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium",
    "Hafnium", "Tantal", "Wolfram", "Rhenium", "Osmium", "Iridium", "Platin",
    "Gold", "Quecksilber", "Thallium", "Blei", "Bismut", "Polonium", "Astat",
    "Radon", "Francium", "Radium", "Actinium", "Thorium", "Protactinium",
    "Uran", "Neptunium", "Plutonium", "Americium", "Curium", "Berkelium",
    "Californium", "Einsteinium", "Fermium", "Mendelevium", "Nobelium",
    "Lawrencium", "Rutherfordium", "Dubnium", "Seaborgium", "Bohrium",
    "Hassium", "Meitnerium", "Darmstadtium", "Roentgenium", "Copernicium",
    "Nihonium", "Flerovium", "Moscovium", "Livermorium", "Tenness",
    "Oganesson"];


fn main() {
    let symbols_lower: Vec<String> = SYMBOLS.iter().map(|symbol| symbol.to_ascii_lowercase()).collect();

    let mut line = String::new();
    io::stdin()
        .read_line(&mut line)
        .expect("Failed to read line");

    match elementify(line.trim(), &symbols_lower[..]) {
        Some(indices) => {
            for i in indices.iter().cloned() {
                print!("{}", SYMBOLS[i]);
            }
            println!("");

            for i in indices.iter().cloned() {
                print!("{} ", ELEMENTS[i]);
            }
            println!("");

            for i in indices.iter() {
                print!("{} ", i);
            }
            println!("");
        },
        None => println!("<not possible>")
    }
}


fn elementify(s: &str, symbols: &[String]) -> Option<Vec<usize>> {
    let v = backtrack(&s.to_ascii_lowercase(), symbols, true);
    v.map(|mut v| {v.reverse(); v})
}

fn backtrack(s: &str, symbols: &[String], can_skip: bool) -> Option<Vec<usize>> {
    if s.len() == 0 {
        return Some(Vec::new());
    }

    // first try without skipping
    for (i, symbol) in symbols.iter().enumerate() {
        if s.starts_with(symbol) {
            if let Some(mut v) = backtrack(&s[symbol.len()..], symbols, false) {
                v.push(i);
                return Some(v);
            }
        }
    }

    // if we're not allowed to skip, there is no solution
    if !can_skip {
        return None;
    }

    // if we can skip try again with skipping
    for (i, symbol) in symbols.iter().enumerate() {
        if s.starts_with(symbol) {
            if let Some(mut v) = backtrack(&s[symbol.len()..], symbols, true) {
                v.push(i);
                return Some(v);
            }
        }
    }

    // else we skip
    let mut v = backtrack(&s[1..], symbols, true).expect("Backtracking after skipping failed");
    v.push(0);
    return Some(v);
}
