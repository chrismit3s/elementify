symbols = [
    "_",
    "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al",
    "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe",
    "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr",
    "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn",
    "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm",
    "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W",
    "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn",
    "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf",
    "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds",
    "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og"]

elements = [
    "<missing>",
    "Wasserstoff", "Helium", "Lithium", "Beryllium", "Bor", "Kohlenstoff",
    "Stickstoff", "Sauerstoff", "Fluor", "Neon", "Natrium", "Magnesium",
    "Aluminium", "Silicium", "Phosphor", "Schwefel", "Chlor", "Argon",
    "Kalium", "Calcium", "Scandium", "Titan", "Vanadium", "Chrom", "Mangan",
    "Eisen", "Cobalt", "Nickel", "Kupfer", "Zink", "Gallium", "Germanium",
    "Arsen", "Selen", "Brom", "Krypton", "Rubidium", "Strontium", "Yttrium",
    "Zirconium", "Niob", "Molybdän", "Technetium", "Ruthenium", "Rhodium",
    "Palladium", "Silber", "Cadmium", "Indium", "Zinn", "Antimon", "Tellur",
    "Iod", "Xenon", "Caesium", "Barium", "Lanthan", "Cer", "Praseodym",
    "Neodym", "Promethium", "Samarium", "Europium", "Gadolinium", "Terbium",
    "Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium",
    "Hafnium", "Tantal", "Wolfram", "Rhenium", "Osmium", "Iridium", "Platin",
    "Gold", "Quecksilber", "Thallium", "Blei", "Bismut", "Polonium", "Astat",
    "Radon", "Francium", "Radium", "Actinium", "Thorium", "Protactinium",
    "Uran", "Neptunium", "Plutonium", "Americium", "Curium", "Berkelium",
    "Californium", "Einsteinium", "Fermium", "Mendelevium", "Nobelium",
    "Lawrencium", "Rutherfordium", "Dubnium", "Seaborgium", "Bohrium",
    "Hassium", "Meitnerium", "Darmstadtium", "Roentgenium", "Copernicium",
    "Nihonium", "Flerovium", "Moscovium", "Livermorium", "Tenness",
    "Oganesson"]


def elementify(s, symbols):
    return backtrack(s.lower(), symbols, True)


def backtrack(s, symbols, can_skip):
    if len(s) == 0:
        return []

    # first try without skipping
    for i, symbol in enumerate(symbols):
        if not s.startswith(symbol):
            continue

        ret = backtrack(s[len(symbol):], symbols, False)
        if ret is not None:
            return [i] + ret

    # if we're not allowed to skip, there is no solution
    if not can_skip:
        return None

    # if we can skip try again with skipping
    for i, symbol in enumerate(symbols):
        if not s.startswith(symbol):
            continue

        ret = backtrack(s[len(symbol):], symbols, True)
        if ret is not None:
            return [i] + ret

    # else we skip
    ret = backtrack(s[1:], symbols, True)
    return [i] + ret


def main():
    symbols_lower = [s.lower() for s in symbols]

    s = input()
    indices = elementify(s, symbols_lower)
    if indices is None:
        print("<not possible>")
    else:
        print("".join(symbols[i] for i in indices))
        print(" ".join(elements[i] for i in indices))


if __name__ == "__main__":
    main()
