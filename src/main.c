#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


#define NUM_SYMBOLS 119
#define SENTINEL (-1)
#define BUF_SIZE 128


const char *symbols[NUM_SYMBOLS] = { "_",
    "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al",
    "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe",
    "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr",
    "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn",
    "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm",
    "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W",
    "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn",
    "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf",
    "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds",
    "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts", "Og" };

const char *elements[NUM_SYMBOLS] = { "<missing>",
    "Wasserstoff", "Helium", "Lithium", "Beryllium", "Bor", "Kohlenstoff",
    "Stickstoff", "Sauerstoff", "Fluor", "Neon", "Natrium", "Magnesium",
    "Aluminium", "Silicium", "Phosphor", "Schwefel", "Chlor", "Argon",
    "Kalium", "Calcium", "Scandium", "Titan", "Vanadium", "Chrom", "Mangan",
    "Eisen", "Cobalt", "Nickel", "Kupfer", "Zink", "Gallium", "Germanium",
    "Arsen", "Selen", "Brom", "Krypton", "Rubidium", "Strontium", "Yttrium",
    "Zirconium", "Niob", "Molybdän", "Technetium", "Ruthenium", "Rhodium",
    "Palladium", "Silber", "Cadmium", "Indium", "Zinn", "Antimon", "Tellur",
    "Iod", "Xenon", "Caesium", "Barium", "Lanthan", "Cer", "Praseodym",
    "Neodym", "Promethium", "Samarium", "Europium", "Gadolinium", "Terbium",
    "Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium",
    "Hafnium", "Tantal", "Wolfram", "Rhenium", "Osmium", "Iridium", "Platin",
    "Gold", "Quecksilber", "Thallium", "Blei", "Bismut", "Polonium", "Astat",
    "Radon", "Francium", "Radium", "Actinium", "Thorium", "Protactinium",
    "Uran", "Neptunium", "Plutonium", "Americium", "Curium", "Berkelium",
    "Californium", "Einsteinium", "Fermium", "Mendelevium", "Nobelium",
    "Lawrencium", "Rutherfordium", "Dubnium", "Seaborgium", "Bohrium",
    "Hassium", "Meitnerium", "Darmstadtium", "Roentgenium", "Copernicium",
    "Nihonium", "Flerovium", "Moscovium", "Livermorium", "Tenness",
    "Oganesson" };


int *elementify(const char *s, const char *symbols[]);
int backtrack(const char *s, int *res, const char *symbols[], bool can_skip);
bool startswith(const char *s, const char *prefix);
char lower_char(char s);
char *lower_str(const char *s);
void read_line(char *buf, int size);


int main(void) {
	char *symbols_lower[NUM_SYMBOLS] = { 0 };
	for (int i = 0; i != NUM_SYMBOLS; ++i)
		symbols_lower[i] = lower_str(symbols[i]);

	char buf[BUF_SIZE] = "";
	read_line(buf, BUF_SIZE);
	int *indices = elementify(buf, (const char **)symbols_lower);

	if (indices == NULL) {
		printf("<not possible>\n");
	}
	else {
		for (int *ptr = indices; *ptr != SENTINEL; ++ptr)
			printf("%s", symbols[*ptr]);
		printf("\n");
		for (int *ptr = indices; *ptr != SENTINEL; ++ptr)
			printf("%s ", elements[*ptr]);
		printf("\n");
		for (int *ptr = indices; *ptr != SENTINEL; ++ptr)
			printf("%d ", *ptr);
		printf("\n");
	}

	free(indices);
	for (int i = 0; i != NUM_SYMBOLS; ++i)
		free(symbols_lower[i]);
	return EXIT_SUCCESS;
}

int *elementify(const char *s, const char *symbols[]) {
	s = lower_str(s);  // returns a new lowercase string
	int *indices = calloc(strlen(s) + 1, sizeof(int));  // worst-case: every symbol only has one character, +1 for sentinel
	int last = backtrack(s, indices, symbols, true);
	if (last == -1) {
		return NULL;
	}
	else {
		indices[last] = SENTINEL;
		return indices;
	}
}

int backtrack(const char *s, int *res, const char *symbols[], bool can_skip) {
	int len = strlen(s);
	if (len == 0)
		return 0;

	// first try without skipping
	for (int i = 1; i != NUM_SYMBOLS; ++i) {
		if (!startswith(s, symbols[i]))
			continue;

		int symbol_len = strlen(symbols[i]);
		int ret = backtrack(s + symbol_len, res + 1, symbols, false);
		if (ret != -1) {
			*res = i;
			return ret + 1;
		}
	}

	// if we're not allowed to skip, there is no solution
	if (!can_skip)
		return -1;

	// if we can skip try again with skipping
	for (int i = 1; i != NUM_SYMBOLS; ++i) {
		if (!startswith(s, symbols[i]))
			continue;

		int symbol_len = strlen(symbols[i]);
		int ret = backtrack(s + symbol_len, res + 1, symbols, true);
		if (ret != -1) {
			*res = i;
			return ret + 1;
		}
	}

	// else we skip
	int ret = backtrack(s + 1, res + 1, symbols, true);
	*res = 0;
	return ret + 1;
}

bool startswith(const char *s, const char *prefix) {
	int i;
	for (i = 0; s[i] != '\0' && prefix[i] != '\0'; ++i)
		if (s[i] != prefix[i])
			return false;
	return prefix[i] == '\0';  // if len(prefix) <= len(s) return true
}

char lower_char(char s) {
	return s + ('a' - 'A') * ('A' <= s && s <= 'Z');  // branchless to lower
}

char *lower_str(const char *s) {
	int size = strlen(s) + 1;
	char *res = malloc(size);
	memcpy(res, s, size);

	for (char *ptr = res; *ptr != '\0'; ++ptr)
		*ptr = lower_char(*ptr);
	return res;
}

void read_line(char *buf, int size) {
	char c;
	int i = 0;
	while ((c = getchar()) != '\n' && i != size - 1)
		buf[i++] = c;
	buf[i] = '\0';
}
