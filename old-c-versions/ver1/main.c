#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#define LEN		118
#define LIST_SZ	100
#define BUF_SZ	LIST_SZ

struct element{
	char *name;
	char *symbol;
};

const struct element elements[] = {
	{ "non alpha char",	" "	 },
	{ "Wasserstoff", 	"H"  },
	{ "Helium", 		"He" },
	{ "Lithium", 		"Li" },
	{ "Beryllium", 		"Be" },
	{ "Bor", 			"B", },
	{ "Kohlenstoff", 	"C", },
	{ "Stickstoff", 	"N", },
	{ "Sauerstoff", 	"O", },
	{ "Fluor", 			"F", },
	{ "Neon", 			"Ne" },
	{ "Natrium", 		"Na" },
	{ "Magnesium", 		"Mg" },
	{ "Aluminium", 		"Al" },
	{ "Silicium", 		"Si" },
	{ "Phosphor", 		"P"  },
	{ "Schwefel", 		"S"  },
	{ "Chlor", 			"Cl" },
	{ "Argon", 			"Ar" },
	{ "Kalium",			"K"  },
	{ "Calcium",	 	"Ca" },
	{ "Scandium", 		"Sc" },
	{ "Titan", 			"Ti" },
	{ "Vanadium", 		"V"  },
	{ "Chrom", 			"Cr" },
	{ "Mangan", 		"Mn" },
	{ "Eisen",			"Fe" },
	{ "Cobalt", 		"Co" },
	{ "Nickel", 		"Ni" },
	{ "Kupfer", 		"Cu" },
	{ "Zink", 			"Zn" },
	{ "Gallium", 		"Ga" },
	{ "Germanium", 		"Ge" },
	{ "Arsen", 			"As" },
	{ "Selen", 			"Se" },
	{ "Brom", 			"Br" },
	{ "Krypton", 		"Kr" },
	{ "Rubidium", 		"Rb" },
	{ "Strontium", 		"Sr" },
	{ "Yttrium", 		"Y", },
	{ "Zirconium", 		"Zr" },
	{ "Niob", 			"Nb" },
	{ "Molybd�n", 		"Mo" },
	{ "Technetium", 	"Tc" },
	{ "Ruthenium", 		"Ru" },
	{ "Rhodium", 		"Rh" },
	{ "Palladium", 		"Pd" },
	{ "Silber", 		"Ag" },
	{ "Cadmium", 		"Cd" },
	{ "Indium", 		"In" },
	{ "Zinn", 			"Sn" },
	{ "Antimon", 		"Sb" },
	{ "Tellur", 		"Te" },
	{ "Iod", 			"I"  },
	{ "Xenon", 			"Xe" },
	{ "Caesium", 		"Cs" },
	{ "Barium", 		"Ba" },
	{ "Lanthan", 		"La" },
	{ "Cer", 			"Ce" },
	{ "Praseodym", 		"Pr" },
	{ "Neodym", 		"Nd" },
	{ "Promethium", 	"Pm" },
	{ "Samarium", 		"Sm" },
	{ "Europium", 		"Eu" },
	{ "Gadolinium", 	"Gd" },
	{ "Terbium", 		"Tb" },
	{ "Dysprosium", 	"Dy" },
	{ "Holmium", 		"Ho" },
	{ "Erbium", 		"Er" },
	{ "Thulium", 		"Tm" },
	{ "Ytterbium", 		"Yb" },
	{ "Lutetium", 		"Lu" },
	{ "Hafnium", 		"Hf" },
	{ "Tantal", 		"Ta" },
	{ "Wolfram", 		"W"  },
	{ "Rhenium", 		"Re" },
	{ "Osmium", 		"Os" },
	{ "Iridium", 		"Ir" },
	{ "Platin", 		"Pt" },
	{ "Gold", 			"Au" },
	{ "Quecksilber", 	"Hg" },
	{ "Thallium", 		"Tl" },
	{ "Blei", 			"Pb" },
	{ "Bismut", 		"Bi" },
	{ "Polonium", 		"Po" },
	{ "Astat", 			"At" },
	{ "Radon", 			"Rn" },
	{ "Francium", 		"Fr" },
	{ "Radium", 		"Ra" },
	{ "Actinium", 		"Ac" },
	{ "Thorium", 		"Th" },
	{ "Protactinium", 	"Pa" },
	{ "Uran", 			"U"  },
	{ "Neptunium", 		"Np" },
	{ "Plutonium", 		"Pu" },
	{ "Americium", 		"Am" },
	{ "Curium", 		"Cm" },
	{ "Berkelium", 		"Bk" },
	{ "Californium", 	"Cf" },
	{ "Einsteinium", 	"Es" },
	{ "Fermium", 		"Fm" },
	{ "Mendelevium", 	"Md" },
	{ "Nobelium", 		"No" },
	{ "Lawrencium", 	"Lr" },
	{ "Rutherfordium", 	"Rf" },
	{ "Dubnium", 		"Db" },
	{ "Seaborgium", 	"Sg" },
	{ "Bohrium", 		"Bh" },
	{ "Hassium", 		"Hs" },
	{ "Meitnerium", 	"Mt" },
	{ "Darmstadtium", 	"Ds" },
	{ "Roentgenium", 	"Rg" },
	{ "Copernicium", 	"Cn" },
	{ "Nihonium", 		"Nh" },
	{ "Flerovium", 		"Fl" },
	{ "Moscovium", 		"Mc" },
	{ "Livermorium", 	"Lv" },
	{ "Tenness", 		"Ts" },
	{ "Oganesson", 		"Og" }
};

bool elementify(const char *str, int element_list[], int len);
int read_line(char *buf, int n);
int compare(const char *str1, const char *str2);

int main(void) {
	char str[BUF_SZ] = "";
	read_line(str, BUF_SZ);
	printf("%s\n", str);
	
	int list[LIST_SZ] = { 0 };
	bool is_complete = elementify(str, list, LIST_SZ);
	
	printf(is_complete ? "Complete: \n" : "Uncomplete: \n");

	int i;
	for (i = 0; i != LIST_SZ; ++i)
		printf("%s", elements[list[i]].symbol);
	printf("\n");

	for (i = 0; i != LIST_SZ; ++i) 
		if (list[i] != 0)
			printf("%s ", elements[list[i]].name);
	printf("\n");

	return EXIT_SUCCESS;
}

bool elementify(const char *str, int element_list[], int len) {
	int i;

	//terminate
	if (len <= 0 || str[0] == '\0')
		return true;

	//skip not alphabetic characters
	if(!isalpha(str[0]))
		elementify(str + 1, element_list, len);

	//look for element symbols
	for (i = 0; i != LEN; ++i) {
		if (compare(elements[i].symbol, str) == 0) {
			element_list[0] = i;
			return elementify(str + strlen(elements[i].symbol), element_list + 1, len - 1);
		}
	}

	elementify(str + 1, element_list, len);
	return false;
}

int read_line(char *buf, int n) {
	char c;
	int chars_read = 0;
	
	while (chars_read < n - 1 && (c = getchar()) != '\n')
		buf[chars_read++] = c;

	buf[chars_read++] = '\0';
	
	return chars_read;
}

int compare(const char *str1, const char *str2) {
	int i;
	for (i = 0; str1[i] != '\0' && str2[i] != '\0'; ++i) {
		if (tolower(str1[i]) == tolower(str2[i]))
			continue;
		else if (tolower(str1[i]) > tolower(str2[i]))
			return 1;
		else
			return -1;
	}
	return 0;
}
