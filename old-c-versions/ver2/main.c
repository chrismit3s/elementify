#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#define  PSE_SZ 119
#define  BUF_SZ  81
#define LIST_SZ  80

struct element {
	const char *name;
	const char *symbol;
	int atom_num;
};
const struct element elements[PSE_SZ] = {
	{ "",				"", 	  0 },//just here so that atomic number = index
	{ "Wasserstoff", 	"H" , 	  1 },
	{ "Helium", 		"He", 	  2 },
	{ "Lithium", 		"Li", 	  3 },
	{ "Beryllium", 		"Be", 	  4 },
	{ "Bor", 			"B" , 	  5 },
	{ "Kohlenstoff", 	"C" , 	  6 },
	{ "Stickstoff", 	"N" , 	  7 },
	{ "Sauerstoff", 	"O" , 	  8 },
	{ "Fluor", 			"F" , 	  9 },
	{ "Neon", 			"Ne", 	 10 },
	{ "Natrium", 		"Na", 	 11 },
	{ "Magnesium", 		"Mg", 	 12 },
	{ "Aluminium", 		"Al", 	 13 },
	{ "Silicium", 		"Si", 	 14 },
	{ "Phosphor", 		"P" , 	 15 },
	{ "Schwefel", 		"S" , 	 16 },
	{ "Chlor", 			"Cl", 	 17 },
	{ "Argon", 			"Ar", 	 18 },
	{ "Kalium", 		"K" , 	 19 },
	{ "Calcium", 		"Ca", 	 20 },
	{ "Scandium", 		"Sc", 	 21 },
	{ "Titan", 			"Ti", 	 22 },
	{ "Vanadium", 		"V" , 	 23 },
	{ "Chrom", 			"Cr", 	 24 },
	{ "Mangan", 		"Mn", 	 25 },
	{ "Eisen", 			"Fe", 	 26 },
	{ "Cobalt", 		"Co", 	 27 },
	{ "Nickel", 		"Ni", 	 28 },
	{ "Kupfer", 		"Cu", 	 29 },
	{ "Zink", 			"Zn", 	 30 },
	{ "Gallium", 		"Ga", 	 31 },
	{ "Germanium", 		"Ge", 	 32 },
	{ "Arsen", 			"As", 	 33 },
	{ "Selen", 			"Se", 	 34 },
	{ "Brom", 			"Br", 	 35 },
	{ "Krypton", 		"Kr", 	 36 },
	{ "Rubidium", 		"Rb", 	 37 },
	{ "Strontium", 		"Sr", 	 38 },
	{ "Yttrium", 		"Y" , 	 39 },
	{ "Zirconium", 		"Zr", 	 40 },
	{ "Niob", 			"Nb", 	 41 },
	{ "Molybd�n", 		"Mo", 	 42 },
	{ "Technetium", 	"Tc", 	 43 },
	{ "Ruthenium", 		"Ru", 	 44 },
	{ "Rhodium", 		"Rh", 	 45 },
	{ "Palladium", 		"Pd", 	 46 },
	{ "Silber", 		"Ag", 	 47 },
	{ "Cadmium", 		"Cd", 	 48 },
	{ "Indium", 		"In", 	 49 },
	{ "Zinn", 			"Sn", 	 50 },
	{ "Antimon", 		"Sb", 	 51 },
	{ "Tellur", 		"Te", 	 52 },
	{ "Iod", 			"I" , 	 53 },
	{ "Xenon", 			"Xe", 	 54 },
	{ "Caesium", 		"Cs", 	 55 },
	{ "Barium", 		"Ba", 	 56 },
	{ "Lanthan", 		"La", 	 57 },
	{ "Cer", 			"Ce", 	 58 },
	{ "Praseodym", 		"Pr", 	 59 },
	{ "Neodym", 		"Nd", 	 60 },
	{ "Promethium", 	"Pm", 	 61 },
	{ "Samarium", 		"Sm", 	 62 },
	{ "Europium", 		"Eu", 	 63 },
	{ "Gadolinium", 	"Gd", 	 64 },
	{ "Terbium", 		"Tb", 	 65 },
	{ "Dysprosium", 	"Dy", 	 66 },
	{ "Holmium", 		"Ho", 	 67 },
	{ "Erbium", 		"Er", 	 68 },
	{ "Thulium", 		"Tm", 	 69 },
	{ "Ytterbium", 		"Yb", 	 70 },
	{ "Lutetium", 		"Lu", 	 71 },
	{ "Hafnium", 		"Hf", 	 72 },
	{ "Tantal", 		"Ta", 	 73 },
	{ "Wolfram", 		"W" , 	 74 },
	{ "Rhenium", 		"Re", 	 75 },
	{ "Osmium", 		"Os", 	 76 },
	{ "Iridium", 		"Ir", 	 77 },
	{ "Platin", 		"Pt", 	 78 },
	{ "Gold", 			"Au", 	 79 },
	{ "Quecksilber", 	"Hg", 	 80 },
	{ "Thallium", 		"Tl", 	 81 },
	{ "Blei", 			"Pb", 	 82 },
	{ "Bismut", 		"Bi", 	 83 },
	{ "Polonium", 		"Po", 	 84 },
	{ "Astat", 			"At", 	 85 },
	{ "Radon", 			"Rn", 	 86 },
	{ "Francium", 		"Fr", 	 87 },
	{ "Radium", 		"Ra", 	 88 },
	{ "Actinium", 		"Ac", 	 89 },
	{ "Thorium", 		"Th", 	 90 },
	{ "Protactinium", 	"Pa", 	 91 },
	{ "Uran", 			"U" , 	 92 },
	{ "Neptunium", 		"Np", 	 93 },
	{ "Plutonium", 		"Pu", 	 94 },
	{ "Americium", 		"Am", 	 95 },
	{ "Curium", 		"Cm", 	 96 },
	{ "Berkelium", 		"Bk", 	 97 },
	{ "Californium", 	"Cf", 	 98 },
	{ "Einsteinium", 	"Es", 	 99 },
	{ "Fermium", 		"Fm", 	100 },
	{ "Mendelevium", 	"Md", 	101 },
	{ "Nobelium", 		"No", 	102 },
	{ "Lawrencium", 	"Lr", 	103 },
	{ "Rutherfordium", 	"Rf", 	104 },
	{ "Dubnium", 		"Db", 	105 },
	{ "Seaborgium", 	"Sg", 	106 },
	{ "Bohrium", 		"Bh", 	107 },
	{ "Hassium", 		"Hs", 	108 },
	{ "Meitnerium", 	"Mt", 	109 },
	{ "Darmstadtium", 	"Ds", 	110 },
	{ "Roentgenium", 	"Rg", 	111 },
	{ "Copernicium", 	"Cn", 	112 },
	{ "Nihonium", 		"Nh", 	113 },
	{ "Flerovium", 		"Fl", 	114 },
	{ "Moscovium", 		"Mc", 	115 },
	{ "Livermorium", 	"Lv", 	116 },
	{ "Tenness", 		"Ts", 	117 },
	{ "Oganesson", 		"Og", 	118 }
};

int  read_line(char *buf, int n);
void print_element_name(const struct element *x);
void print_element_symbol(const struct element *x);
int  is_start_of(const char *start, const char *str);
int  elementify(const char *str, const struct element *list[LIST_SZ], int size);
void copy_list(const struct element *dest[LIST_SZ], const struct element *source[LIST_SZ]);

int main(void) {
	printf("DOESNT WORK - TRY ELEMENTIFY3 INSTEAD\n\n");

	//input
	printf("Enter text to elementify:\n   ");
	char str[BUF_SZ] = "";
	read_line(str, BUF_SZ);

	//processing
	const struct element *list[LIST_SZ] = { 0 };
	int skipped = elementify(str, list, LIST_SZ);

	//output
	printf("Elementified:\n   ");
	int i;
	for (i = 0; i != LIST_SZ; ++i)
		print_element_symbol(list[i]);
	printf("\n   ");
	for (i = 0; i != LIST_SZ; ++i)
		print_element_name(list[i]);
	printf("\n");

	return EXIT_SUCCESS;
}

int read_line(char *buf, int n) {
	char c;
	int chars_read = 0;

	while (chars_read < n - 1 && (c = getchar()) != '\n')
		buf[chars_read++] = c;

	buf[chars_read++] = '\0';

	return chars_read;
}

void print_element_name(const struct element *x) {
	if (x != 0)
		printf("%s(%i) ", x->name, x->atom_num);
}

void print_element_symbol(const struct element *x) {
	if (x != 0)
		printf("%s", x->symbol);
}

int is_start_of(const char *start, const char *str) {
	int i1 = 0, i2 = 0;

	//start must be shorter than str 
	if (strlen(start) > strlen(str))
		return -1;

	//since start is shorter, it ends first
	while (start[i1] != '\0') {
		//find next alphabetic character
		for (; !isalpha(start[i1]); ++i1);
		for (; !isalpha(str[i2]); ++i2);
		
		//compare
		if (tolower(start[i1]) == tolower(str[i2])) {
			//next character
			++i1;
			++i2;
			continue;
		}
		else
			return -1;

		//next character
		++i1;
		++i2;
	}

	return i2;
}

int elementify(const char *str, const struct element *list[LIST_SZ], int size) {
	//termination
	if (size <= 0 || str[0] == '\0')
		return 0;

	//skip non alphabetic character
	if (!isalpha(str[0]))
		return elementify(str + 1, list, size);

	//go through pse and look for fitting symbols
	int i;
	int chars_skipped;
	int return_value, best_return_value = INT_MAX;
	const struct element *best_list[LIST_SZ] = { 0 };
	for (i = 1; i != PSE_SZ; ++i) {
		if ((chars_skipped = is_start_of(elements[i].symbol, str)) != -1) {
			//add element to list
			list[0] = elements + i;
			return_value = elementify(str + chars_skipped, list + 1, size - 1);
			
			//update best
			if (return_value < best_return_value) {
				best_return_value = return_value;
				copy_list(best_list, list + 1);
			}
		}
	}

	//skip first letter
	if (chars_skipped == -1)
		return 1 + elementify(str + 1, list, size);
	else {
		copy_list(list, best_list);
		return best_return_value;
	}
}

void copy_list(const struct element *dest[LIST_SZ], const struct element *source[LIST_SZ]) {
	int i;
	for (i = 0; source[i] != NULL; ++i)
		dest[i] = source[i];
	return;
}
