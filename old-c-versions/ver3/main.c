#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#define  PSE_SZ 119
#define  BUF_SZ 81
#define LIST_SZ 80

struct element {
	const char *name;
	const char *symbol;
	int atom_num;
};
const struct element elements[PSE_SZ] = {
	{ "",				"", 	  0 },//just here so that atomic number = index
	{ "Wasserstoff", 	"H" , 	  1 },
	{ "Helium", 		"He", 	  2 },
	{ "Lithium", 		"Li", 	  3 },
	{ "Beryllium", 		"Be", 	  4 },
	{ "Bor", 			"B" , 	  5 },
	{ "Kohlenstoff", 	"C" , 	  6 },
	{ "Stickstoff", 	"N" , 	  7 },
	{ "Sauerstoff", 	"O" , 	  8 },
	{ "Fluor", 			"F" , 	  9 },
	{ "Neon", 			"Ne", 	 10 },
	{ "Natrium", 		"Na", 	 11 },
	{ "Magnesium", 		"Mg", 	 12 },
	{ "Aluminium", 		"Al", 	 13 },
	{ "Silicium", 		"Si", 	 14 },
	{ "Phosphor", 		"P" , 	 15 },
	{ "Schwefel", 		"S" , 	 16 },
	{ "Chlor", 			"Cl", 	 17 },
	{ "Argon", 			"Ar", 	 18 },
	{ "Kalium", 		"K" , 	 19 },
	{ "Calcium", 		"Ca", 	 20 },
	{ "Scandium", 		"Sc", 	 21 },
	{ "Titan", 			"Ti", 	 22 },
	{ "Vanadium", 		"V" , 	 23 },
	{ "Chrom", 			"Cr", 	 24 },
	{ "Mangan", 		"Mn", 	 25 },
	{ "Eisen", 			"Fe", 	 26 },
	{ "Cobalt", 		"Co", 	 27 },
	{ "Nickel", 		"Ni", 	 28 },
	{ "Kupfer", 		"Cu", 	 29 },
	{ "Zink", 			"Zn", 	 30 },
	{ "Gallium", 		"Ga", 	 31 },
	{ "Germanium", 		"Ge", 	 32 },
	{ "Arsen", 			"As", 	 33 },
	{ "Selen", 			"Se", 	 34 },
	{ "Brom", 			"Br", 	 35 },
	{ "Krypton", 		"Kr", 	 36 },
	{ "Rubidium", 		"Rb", 	 37 },
	{ "Strontium", 		"Sr", 	 38 },
	{ "Yttrium", 		"Y" , 	 39 },
	{ "Zirconium", 		"Zr", 	 40 },
	{ "Niob", 			"Nb", 	 41 },
	{ "Molybd�n", 		"Mo", 	 42 },
	{ "Technetium", 	"Tc", 	 43 },
	{ "Ruthenium", 		"Ru", 	 44 },
	{ "Rhodium", 		"Rh", 	 45 },
	{ "Palladium", 		"Pd", 	 46 },
	{ "Silber", 		"Ag", 	 47 },
	{ "Cadmium", 		"Cd", 	 48 },
	{ "Indium", 		"In", 	 49 },
	{ "Zinn", 			"Sn", 	 50 },
	{ "Antimon", 		"Sb", 	 51 },
	{ "Tellur", 		"Te", 	 52 },
	{ "Iod", 			"I" , 	 53 },
	{ "Xenon", 			"Xe", 	 54 },
	{ "Caesium", 		"Cs", 	 55 },
	{ "Barium", 		"Ba", 	 56 },
	{ "Lanthan", 		"La", 	 57 },
	{ "Cer", 			"Ce", 	 58 },
	{ "Praseodym", 		"Pr", 	 59 },
	{ "Neodym", 		"Nd", 	 60 },
	{ "Promethium", 	"Pm", 	 61 },
	{ "Samarium", 		"Sm", 	 62 },
	{ "Europium", 		"Eu", 	 63 },
	{ "Gadolinium", 	"Gd", 	 64 },
	{ "Terbium", 		"Tb", 	 65 },
	{ "Dysprosium", 	"Dy", 	 66 },
	{ "Holmium", 		"Ho", 	 67 },
	{ "Erbium", 		"Er", 	 68 },
	{ "Thulium", 		"Tm", 	 69 },
	{ "Ytterbium", 		"Yb", 	 70 },
	{ "Lutetium", 		"Lu", 	 71 },
	{ "Hafnium", 		"Hf", 	 72 },
	{ "Tantal", 		"Ta", 	 73 },
	{ "Wolfram", 		"W" , 	 74 },
	{ "Rhenium", 		"Re", 	 75 },
	{ "Osmium", 		"Os", 	 76 },
	{ "Iridium", 		"Ir", 	 77 },
	{ "Platin", 		"Pt", 	 78 },
	{ "Gold", 			"Au", 	 79 },
	{ "Quecksilber", 	"Hg", 	 80 },
	{ "Thallium", 		"Tl", 	 81 },
	{ "Blei", 			"Pb", 	 82 },
	{ "Bismut", 		"Bi", 	 83 },
	{ "Polonium", 		"Po", 	 84 },
	{ "Astat", 			"At", 	 85 },
	{ "Radon", 			"Rn", 	 86 },
	{ "Francium", 		"Fr", 	 87 },
	{ "Radium", 		"Ra", 	 88 },
	{ "Actinium", 		"Ac", 	 89 },
	{ "Thorium", 		"Th", 	 90 },
	{ "Protactinium", 	"Pa", 	 91 },
	{ "Uran", 			"U" , 	 92 },
	{ "Neptunium", 		"Np", 	 93 },
	{ "Plutonium", 		"Pu", 	 94 },
	{ "Americium", 		"Am", 	 95 },
	{ "Curium", 		"Cm", 	 96 },
	{ "Berkelium", 		"Bk", 	 97 },
	{ "Californium", 	"Cf", 	 98 },
	{ "Einsteinium", 	"Es", 	 99 },
	{ "Fermium", 		"Fm", 	100 },
	{ "Mendelevium", 	"Md", 	101 },
	{ "Nobelium", 		"No", 	102 },
	{ "Lawrencium", 	"Lr", 	103 },
	{ "Rutherfordium", 	"Rf", 	104 },
	{ "Dubnium", 		"Db", 	105 },
	{ "Seaborgium", 	"Sg", 	106 },
	{ "Bohrium", 		"Bh", 	107 },
	{ "Hassium", 		"Hs", 	108 },
	{ "Meitnerium", 	"Mt", 	109 },
	{ "Darmstadtium", 	"Ds", 	110 },
	{ "Roentgenium", 	"Rg", 	111 },
	{ "Copernicium", 	"Cn", 	112 },
	{ "Nihonium", 		"Nh", 	113 },
	{ "Flerovium", 		"Fl", 	114 },
	{ "Moscovium", 		"Mc", 	115 },
	{ "Livermorium", 	"Lv", 	116 },
	{ "Tenness", 		"Ts", 	117 },
	{ "Oganesson", 		"Og", 	118 }
};

int read_line(char *buf, int n);
int  elementify(const char *str, int * const list, int size);
int _elementify(const char *str, int * const list, int size);
bool is_start_of(const char *start, const char *str);

int main(void) {
	//input
	printf("Enter text to elementify:\n   ");
	char str[BUF_SZ] = "";
	read_line(str, BUF_SZ);

	//processing
	int list[LIST_SZ] = { 0 };
	int skipped = elementify(str, list, LIST_SZ);

	//output
	printf("Elementified:\n   ");
	int i;
	for (i = 0; list[i] != 0; ++i)
		printf("%s", elements[list[i]].symbol);
	printf("\n   ");
	for (i = 0; list[i] != 0; ++i)
		printf("%s[%i] ", elements[list[i]].name, elements[list[i]].atom_num);
	printf("(%i skipped)\n", skipped);
	
	return EXIT_SUCCESS;
}

int read_line(char *buf, int n) {
	char c;
	int chars_read = 0;

	while (chars_read < n - 1 && (c = getchar()) != '\n')
		buf[chars_read++] = c;

	buf[chars_read++] = '\0';

	return chars_read;
}

int elementify(const char *str, int * const list, int size) {
	//copy and "clean" str
	char clean_str[BUF_SZ] = "";
	int i = 0;
	while (*str != '\0') {
		if (isalpha(*str))
			clean_str[i++] = tolower(*str);
		++str;
	}
	
	return _elementify(clean_str, list, size);
}

/* Bug: 
 * 
 * Try "nidhog", "csdhog", "cszti" or anything that looks like 
 *
 *     2-letter-element made out of two 1-letter-elements (Ni, Cs, Yb, Ho,...)
 *   + any element (H, He, Li, Be, ...).
 * 
 * The  result  will have the last  element  twice at the end.
 * Thats because the first  element will need two spots in the
 * the  list once,  so the last  ones in  postion 3  (index 2)
 * Example "NiLi":
 *
 *     [0] N  (Nitrogen)
 *     [1] I  (Iod)
 *     [2] Li (Lithium)
 * 
 * Now when the first element needs only one spot,  list looks
 * like this:
 *
 *     [0] Ni (Nickel)
 *     [1] Li (Lithium)
 *     [2] Li (Lithium)
 *
 * The last "Li" is there,  because it didnt get overwriten by
 * something.
 * If you  want  more  repetions of the  last  element,   just
 * increase  the  number  of  2-letter-elements  that  can  be 
 * written as 2 1-letter-elements  ("NiNiLi"  would  give  you
 * "NiNiLiLiLi").
 * In general:
 *
 *     n * 2-letter-element e1 made out of two 1-letter-elements
 *   + m * any element e2
 *   = n * e1 + (n + m) * e2
 */
int _elementify(const char *str, int * const list, int size) {
	//termination
	if (size <= 0 || *str == '\0')
		return 0;

	//go through pse and look for fitting symbols
	int i;
	int skips;
	int least_skips = INT_MAX, best_element = -1;
	for (i = 1; i != PSE_SZ; ++i) {
		if (is_start_of(elements[i].symbol, str)) {
			//add to list
			*list = i;
			skips = _elementify(str + strlen(elements[i].symbol), list + 1, size - 1);
			 
			if (skips <= least_skips) {
				least_skips = skips;
				best_element = i;
			}
		}
	}

	//skip one letter if there's no hit
	if (best_element == -1) 
		return 1 + _elementify(str + 1, list, size);

	//else use best
	else {
		*list = best_element;
		return least_skips;
	}
}

bool is_start_of(const char *start, const char *str) {
	//start has to be longer
	if (strlen(start) > strlen(str))
		return false;

	//start ends first
	int i;
	for (i = 0; start[i] != '\0'; ++i)
		if (tolower(start[i]) != tolower(str[i]))
			return false;

	return true;
}
