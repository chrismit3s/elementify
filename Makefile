.PHONY: all main

all: c rust

c: src/main.c
	gcc -o bin/main-c.exe src/main.c

rust: src/main.rs
	rustc -o bin/main-rs.exe src/main.rs
